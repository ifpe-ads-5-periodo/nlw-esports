import express from "express";
const app = express();
app.get("/ads", (request, response) => {
    return response.json([
        { id: 1, name: "anúncio 01" },
        { id: 2, name: "anúncio 02" },
        { id: 3, name: "anúncio 03" },
    ]);
});
app.listen(3333);
